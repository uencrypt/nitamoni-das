#include<iostream>
using namespace std;

class node
{
public:
    int data;
    node *next;
};

class list
{
    node *start;

public:

    list()
    {
        start = NULL;
    }

    void create_ll()
    {
        node *new_node,*current;
        int num;

        cout<<"\nEnter -1 to end\n";
        cout<<"\nEnter data :- ";
        cin>>num;

        while(num!=-1)
        {
            new_node = new node;

            new_node->data = num;
            new_node->next = NULL;

            if(start == NULL)
            {
                start = new_node;
                current = new_node;
            }

            else
            {
                current->next = new_node;
                current = new_node;
            }

            cout<<"\nEnter data :- ";
            cin>>num;
        }

        node *ptr;
        ptr = start;

        while(ptr->next != NULL)
            ptr = ptr->next;

        ptr->next = start;
    }

    void display()
    {
        node *ptr;
        ptr = start;

        cout<<"\n\n";
        while(ptr->next != start)
        {
            cout<<ptr->data<<"\t";
            ptr = ptr->next;
        }
        cout<<ptr->data<<"\n";
    }

    void insert_beg()
    {
        int num;
        node *new_node = new node;

        cout<<"\nEnter data :- ";
        cin>>num;

        new_node->data = num;
        new_node->next = start;

        node *ptr;
        ptr = start;

        while(ptr->next != start)
            ptr = ptr->next;

        ptr->next = new_node;

        start = new_node;
    }

    void insert_last()
    {
        int num;
        node *new_node = new node;

        cout<<"\nEnter data :- ";
        cin>>num;

        new_node->data = num;
        new_node->next = start;

        node *ptr;
        ptr = start;

        while(ptr->next != start)
            ptr = ptr->next;

        ptr->next = new_node;
    }
};
int main()
{

    list l;
    l.create_ll();
    l.display();
    l.insert_beg();
    l.display();
    l.insert_last();
    l.display();
    return 0;
}
